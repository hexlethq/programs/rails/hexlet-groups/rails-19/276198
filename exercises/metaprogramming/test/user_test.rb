# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_user
    attributes = { birthday: '01/01/2021' }
    result_attributes = { name: 'Andrey', birthday: DateTime.parse('01/01/2021'), active: false }

    user = User.new(attributes)
    assert_equal result_attributes, user.attributes
  end

  def test_attribute_accessors # rubocop:disable Metrics/AbcSize
    attributes = { name: 'Vasya', birthday: '01/01/2021', active: true }
    user = User.new attributes

    assert_equal attributes[:name], user.name
    assert_equal DateTime.parse(attributes[:birthday]), user.birthday
    assert_equal attributes[:active], user.active

    new_attributes = { name: 'Kolya', birthday: '02/01/2021', active: false }
    user.name = new_attributes[:name]
    user.birthday = new_attributes[:birthday]
    user.active = new_attributes[:active]

    assert_equal new_attributes[:name], user.name
    assert_equal DateTime.parse(new_attributes[:birthday]), user.birthday
    assert_equal new_attributes[:active], user.active
  end

  def test_posts_not_equals
    user1 = User.new
    user2 = User.new name: 'Vasya'

    assert_equal 'Andrey', user1.name
    assert_equal 'Vasya', user2.name
    refute_equal user1.name, user2.name
  end

  def test_attribute_convert
    user = User.new birthday: '01/01/2021'
    result_attributes = { name: 'Andrey', birthday: DateTime.parse('01/01/2021'), active: false }

    assert_equal result_attributes, user.attributes
    assert_instance_of DateTime, user.birthday
  end
  # END
end
