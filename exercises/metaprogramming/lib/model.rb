# frozen_string_literal: true

require 'date'

module Convertor
  def self.included(klass)
    klass.extend self
  end

  def convert(value, type)
    type && !value.nil? ? send("convert_to_#{type}", value) : value
  end

  private

  def convert_to_string(value)
    value.to_s
  end

  def convert_to_integer(value)
    value.to_i
  end

  def convert_to_datetime(value)
    DateTime.parse(value)
  end

  def convert_to_boolean(value)
    return true if value == 'yes'
    return false if value == 'no'

    !!value
  end
end

# BEGIN
module Model
  attr_reader :attributes

  module ClassMethods
    include Convertor

    def config
      @config ||= {}
    end

    def attribute(name, options = {})
      config[name] = options

      define_method name do
        @attributes[name]
      end

      define_method "#{name}=" do |value|
        @attributes[name] = self.class.convert(value, self.class.config[name][:type])
      end
    end
  end

  def self.included(klass)
    klass.extend ClassMethods
  end

  def initialize(params = {})
    @attributes = {}

    self.class.config.each do |key, options|
      send("#{key}=", options[:default])
    end

    params.each do |key, value|
      send("#{key}=", value) if params.key?(key)
    end
  end
end
