# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  result = {}
  users.select { |item| item[:gender] == 'male' }.each do |item|
    year = item[:birthday].split('-').first
    result[year] = result.fetch(year, 0) + 1
  end

  result
end
# END
