# frozen_string_literal: true

# BEGIN
def get_same_parity(arr)
  arr.filter { |item| arr.first.odd? == item.odd? }
end
# END
