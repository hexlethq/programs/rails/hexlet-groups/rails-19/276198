# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, arr)
  arr.filter do |item|
    item.chars.sort == word.chars.sort
  end
end
# END
