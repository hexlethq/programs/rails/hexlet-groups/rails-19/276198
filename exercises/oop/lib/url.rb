# frozen_string_literal: true

require 'uri'
require 'forwardable'

# BEGIN
class Url
  include Comparable
  extend Forwardable

  def initialize(url)
    @uri = URI url
  end

  def_delegator :@uri, :scheme, :scheme
  def_delegator :@uri, :host, :host

  def query_params
    URI.decode_www_form(@uri.query).to_h.transform_keys(&:to_sym)
  end

  def query_param(key, default_value = nil)
    query_params.fetch(key, default_value)
  end

  protected

  def <=>(other)
    @uri <=> other.uri
  end

  attr_reader :uri
end
# END
