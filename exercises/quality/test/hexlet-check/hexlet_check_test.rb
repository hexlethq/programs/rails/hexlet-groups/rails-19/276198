# frozen_string_literal: true

require 'test_helper'
require_relative '../controllers/bulletins_controller_test'

class HexletCheckTest < ActiveSupport::TestCase
  test 'BulletinsControllerTest exists and has methods' do
    assert defined? BulletinsControllerTest
    test_methods = BulletinsControllerTest.new({}).methods.select { |method| method.start_with? 'test_' }
    assert_not_empty test_methods
  end

  test 'Bulletins fixtures not empty' do
    assert_not_empty Bulletin.all
  end
end
