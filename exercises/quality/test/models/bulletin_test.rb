# == Schema Information
#
# Table name: bulletins
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  published  :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'test_helper'

class BulletinTest < ActiveSupport::TestCase
  setup do
    @published_bulletin = bulletins(:published)
    @unpublished_bulletin = bulletins(:unpublished)
  end

  test 'should be valid bulletin published' do
    assert @published_bulletin.valid?
  end

  test 'should be valid bulletin unpublished' do
    assert @unpublished_bulletin.valid?
  end

  test 'should validate bulletin without title' do
    bulletin = Bulletin.new(body: 'Text 1', published: false)

    assert_not bulletin.valid?, 'bulletin is valid without a title'
    assert_not_nil bulletin.errors[:title], 'no validation error for title present'
  end

  test 'should validate bulletin without body' do
    bulletin = Bulletin.new(title: 'Title 1', published: false)

    assert_not bulletin.valid?, 'bulletin is valid without a title'
    assert_not_nil bulletin.errors[:title], 'no validation error for body present'
  end
end
