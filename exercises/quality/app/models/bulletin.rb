# frozen_string_literal: true

# == Schema Information
#
# Table name: bulletins
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  published  :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Bulletin < ApplicationRecord
  validates :title, presence: true
  validates :body, presence: true
end
