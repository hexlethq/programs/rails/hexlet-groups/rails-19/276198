# frozen_string_literal: true

class BulletinsController < ApplicationController
  def show
    @bulletin = Bulletin.find(params[:id])
  end

  def index
    @bulletins = Bulletin.all
  end
end
