# frozen_string_literal: true

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)

    if req.path.split('/')[1] == 'admin'
      [403, {}, 'Acces Denied']
    else
      @app.call(env)
    end
  end
end
