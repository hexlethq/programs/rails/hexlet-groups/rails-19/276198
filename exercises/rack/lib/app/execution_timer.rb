# frozen_string_literal: true

class ExecutionTime
  def initialize(app)
    @app = app
  end

  def call(env)
    time_start = Time.now
    status, headers, body = @app.call(env)
    time_end = Time.now

    [status, headers, "#{body}!\n#{time_diff_milli(time_start, time_end)}ms"]
  end

  private

  def time_diff_milli(start, finish)
    (finish - start) * 1000.0
  end
end
