# frozen_string_literal: true

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)

    body_hash = Digest::SHA256.hexdigest(body)

    [status, headers, "#{body}!\n#{body_hash}"]
  end
end
