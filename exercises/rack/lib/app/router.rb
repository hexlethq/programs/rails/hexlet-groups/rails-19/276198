# frozen_string_literal: true

class Router
  def call(env)
    req = Rack::Request.new(env)

    resolve_routes(req)
  end

  def resolve_routes(req)
    case req.path
    when '/'
      [200, { 'Content-Type' => 'text/plain' }, 'Hello, World!']
    when '/admin'
      [200, { 'Content-Type' => 'text/plain' }, 'Admin page']
    when '/about'
      [200, { 'Content-Type' => 'text/plain' }, 'About page']
    when '/contact'
      [200, { 'Content-Type' => 'text/plain' }, 'Contact page']
    else
      [404, { 'Content-Type' => 'text/plain' }, '404 Not Found']
    end
  end
end
