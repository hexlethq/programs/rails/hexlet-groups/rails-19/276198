# frozen_string_literal: true

# BEGIN
def compare_versions(version_a, version_b)
  splitted_a = version_a.split('.').map(&:to_i)
  splitted_b = version_b.split('.').map(&:to_i)

  splitted_a.each_with_index.reduce(0) do |accum, (version, index)|
    accum.zero? ? version <=> splitted_b.fetch(index, 0) : accum
  end
end
# END
