# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  text.split.map { |item| stop_words.any? { |word| word == item } ? '$#%!' : item }.join(' ')
  # END
end
