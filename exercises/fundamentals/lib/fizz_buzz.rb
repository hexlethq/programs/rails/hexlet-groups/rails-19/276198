# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  arr = []
  [*start..stop].each do |i|
    result = ''
    result += 'Fizz' if (i % 3).zero?
    result += 'Buzz' if (i % 5).zero?
    result += i.to_s if result.size.zero?
    arr.push(result)
  end
  arr.join(' ')
end
# END
