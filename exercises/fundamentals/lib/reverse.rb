# frozen_string_literal: true

# BEGIN
def reverse(text)
  text_arr = text.chars
  reversed_arr = []
  reversed_arr.push text_arr.pop until text_arr.empty?
  reversed_arr.join
end
# END
